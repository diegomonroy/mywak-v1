<!-- Begin E Part 1 -->
	<section class="e_part_1">
		<div class="row align-center align-middle">
			<div class="small-12 columns text-center">
				<a href="https://www.mywak.com.co" class="a"><img src="<?php echo get_template_directory_uri(); ?>/build/e_part_1_logo_mywak.png" title="MyWak" alt="MyWak"></a>
				<a href="" class="b"><img src="<?php echo get_template_directory_uri(); ?>/build/e_part_1_logo_pedigree.png" title="Pedigree" alt="Pedigree"></a>
			</div>
		</div>
	</section>
<!-- End E Part 1 -->
<!-- Begin E Part 2 -->
	<section class="e_part_2">
		<div class="row">
			<div class="small-12 columns">
				<div class="texts text-center">
					<div class="text_1">¡NOS HEMOS UNIDO!</div>
					<div class="text_2">para ayudarte a educar a</div>
					<div class="text_3">tu mejor amigo perruno</div>
					<div class="line"></div>
					<div class="text_4">Te ofrecemos los</div>
					<div class="text_5">mejores entrenadores</div>
				</div>
			</div>
		</div>
	</section>
<!-- End E Part 2 -->
<!-- Begin E Part 3 -->
	<section class="e_part_3">
		<div class="row collapse">
			<div class="small-12 columns">
				<div class="title_1 text-center">
					<div class="text_1">NUESTROS</div>
					<div class="text_2">BENEFICIOS</div>
				</div>
				<div class="main_landing">
					<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_3.png">
				</div>
				<div class="icon_1 text-center">
					<div class="text">
						Entrenadores<br />
						certificados
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_3_icon_1.png">
					</div>
				</div>
				<div class="icon_2 text-center">
					<div class="text">
						Geolocalización
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_3_icon_2.png">
					</div>
				</div>
				<div class="icon_3 text-center">
					<div class="text">
						Seguro en caso<br />
						de accidente
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_3_icon_3.png">
					</div>
				</div>
				<div class="icon_4 text-center">
					<div class="text">
						Acumula<br />
						wakpuntos
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_3_icon_4.png">
					</div>
				</div>
				<div class="title_2 text-center">
					<div class="text_1">¡SIGUE ESTOS</div>
					<div class="text_2">TRES PASOS!</div>
				</div>
				<div class="icon_5 text-center">
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_3_icon_5.png">
					</div>
					<div class="text">
						<div class="text_1">DILIGENCIA</div>
						<div class="text_2">El formulario</div>
					</div>
				</div>
				<div class="icon_6 text-center">
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_3_icon_6.png">
					</div>
					<div class="text">
						<div class="text_1">DESCARGA</div>
						<div class="text_2">La aplicación</div>
					</div>
				</div>
				<div class="icon_7 text-center">
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_3_icon_7.png">
					</div>
					<div class="text">
						<div class="text_1">COMPRA</div>
						<div class="text_2">Cualquier producto<br />pedigree</div>
					</div>
				</div>
				<div class="line">
					<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_3_line.png">
				</div>
			</div>
		</div>
	</section>
<!-- End E Part 3 -->
<!-- Begin E Part 4 -->
	<section class="e_part_4">
		<div class="row">
			<div class="small-12 columns text-center">
				<div class="text">
					Toma una foto a la factura de compra y envíala<br />
					al whatsapp con tu nombre y teléfono.<br />
					La compra debe ser del mes en curso.
				</div>
			</div>
		</div>
	</section>
<!-- End E Part 4 -->
<!-- Begin E Part 5 -->
	<section class="e_part_5">
		<div class="row align-center align-middle collapse">
			<div class="small-12 medium-6 columns text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_5.png">
			</div>
			<div class="small-12 medium-6 columns">
				<div class="form_landing">
					<?php echo do_shortcode( '[contact-form-7 id="1045" title="Entrenamientos"]' ); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End E Part 5 -->
<!-- Begin E Part 6 -->
	<section class="e_part_6">
		<div class="row">
			<div class="small-12 columns text-center">
				<div class="image">
					<img src="<?php echo get_template_directory_uri(); ?>/build/e_part_6.png">
				</div>
			</div>
		</div>
	</section>
<!-- End E Part 6 -->