<!-- Begin LDH Top -->
	<section class="ldh_top">
		<div class="row collapse">
			<div class="small-12 columns text-center">
				<?php if ( is_page( array( 'anastasioconmywakdoghouser' ) ) ) : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/build/ldh_top_anastasio.png">
				<?php endif; ?>
				<?php if ( is_page( array( 'ateneaconmywakdoghouser' ) ) ) : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/build/ldh_top_atenea.png">
				<?php endif; ?>
				<?php if ( is_page( array( 'fresitaconmywakdoghouser' ) ) ) : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/build/ldh_top_fresita.png">
				<?php endif; ?>
				<?php if ( is_page( array( 'jackconmywakdoghouser' ) ) ) : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/build/ldh_top_jack.png">
				<?php endif; ?>
				<?php if ( is_page( array( 'leahconmywakdoghouser' ) ) ) : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/build/ldh_top_leah.png">
				<?php endif; ?>
				<?php if ( is_page( array( 'matildaconmywakdoghouser' ) ) ) : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/build/ldh_top_matilda.png">
				<?php endif; ?>
				<?php if ( is_page( array( 'pinaconmywakdoghouser' ) ) ) : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/build/ldh_top_pina.png">
				<?php endif; ?>
			</div>
		</div>
	</section>
<!-- End LDH Top -->
<!-- Begin LDH Content -->
	<section class="ldh_content">
		<div class="row">
			<div class="small-12 columns">
				<div class="text-center">
					<span class="title">Solo déjame tus datos</span>
					<span class="subtitle">y nos comunicaremos contigo</span>
				</div>
				<div class="form_landing">
					<?php if ( is_page( array( 'anastasioconmywakdoghouser' ) ) ) : ?>
						<?php echo do_shortcode( '[contact-form-7 id="1207" title="Dog Houser - Anastasio"]' ); ?>
					<?php endif; ?>
					<?php if ( is_page( array( 'ateneaconmywakdoghouser' ) ) ) : ?>
						<?php echo do_shortcode( '[contact-form-7 id="1196" title="Dog Houser - Atenea"]' ); ?>
					<?php endif; ?>
					<?php if ( is_page( array( 'fresitaconmywakdoghouser' ) ) ) : ?>
						<?php echo do_shortcode( '[contact-form-7 id="1197" title="Dog Houser - Fresita"]' ); ?>
					<?php endif; ?>
					<?php if ( is_page( array( 'jackconmywakdoghouser' ) ) ) : ?>
						<?php echo do_shortcode( '[contact-form-7 id="1184" title="Dog Houser - Jack"]' ); ?>
					<?php endif; ?>
					<?php if ( is_page( array( 'leahconmywakdoghouser' ) ) ) : ?>
						<?php echo do_shortcode( '[contact-form-7 id="1208" title="Dog Houser - Leah"]' ); ?>
					<?php endif; ?>
					<?php if ( is_page( array( 'matildaconmywakdoghouser' ) ) ) : ?>
						<?php echo do_shortcode( '[contact-form-7 id="1198" title="Dog Houser - Matilda"]' ); ?>
					<?php endif; ?>
					<?php if ( is_page( array( 'pinaconmywakdoghouser' ) ) ) : ?>
						<?php echo do_shortcode( '[contact-form-7 id="1209" title="Dog Houser - Pina"]' ); ?>
					<?php endif; ?>
				</div>
				<div class="bottom">
					<div class="text-center title">
						RED DE CUIDADORES DE CONFIANZA<br />
						PARA TODO TIPO DE MASCOTAS
					</div>
					<div class="text-center subtitle">
						¿Vas a viajar? ¿Pasas mucho tiempo fuera de casa?<br />
						¿Quieres un trato familiar para tu mascota?
					</div>
					<div class="row icons">
						<div class="small-12 medium-3 columns">
							<p class="text-center"><img src="<?php echo get_template_directory_uri(); ?>/build/ldh_bottom_icon_house.png"></p>
							<p class="text-center">Cuidado como<br />en casa</p>
						</div>
						<div class="small-12 medium-3 columns">
							<p class="text-center"><img src="<?php echo get_template_directory_uri(); ?>/build/ldh_bottom_icon_dog.png"></p>
							<p class="text-center">Menos estrés<br />a la mascota</p>
						</div>
						<div class="small-12 medium-3 columns">
							<p class="text-center"><img src="<?php echo get_template_directory_uri(); ?>/build/ldh_bottom_icon_person.png"></p>
							<p class="text-center">Atención<br />familiar</p>
						</div>
						<div class="small-12 medium-3 columns">
							<p class="text-center"><img src="<?php echo get_template_directory_uri(); ?>/build/ldh_bottom_icon_shield.png"></p>
							<p class="text-center">Seguro de<br />emergencias</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- End LDH Content -->