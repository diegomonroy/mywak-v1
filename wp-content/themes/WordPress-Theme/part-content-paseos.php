<!-- Begin P Part 1 -->
	<section class="p_part_1">
		<div class="row collapse">
			<div class="small-12 medium-6 columns">
				<a href="https://www.mywak.com.co" class="a"><img src="<?php echo get_template_directory_uri(); ?>/build/p_part_1_logo_mywak.png" title="MyWak" alt="MyWak"></a>
				<a href="" class="b"><img src="<?php echo get_template_directory_uri(); ?>/build/p_part_1_logo_pedigree.png" title="Pedigree" alt="Pedigree"></a>
				<div class="texts text-center">
					<div class="text_1">¡NOS HEMOS UNIDO!</div>
					<div class="text_2">Estamos conformando la</div>
					<div class="text_3">manada <strong>mywak pedigree</strong></div>
					<div class="line"></div>
					<div class="text_4">Te ofrecemos los mejores</div>
					<div class="text_5">paseos al mejor precio</div>
				</div>
			</div>
			<div class="small-12 medium-6 columns">
				<div class="image text-center">
					<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_1.png">
				</div>
			</div>
		</div>
	</section>
<!-- End P Part 1 -->
<!-- Begin P Part 2 -->
	<section class="p_part_2">
		<div class="row">
			<div class="small-12 columns">
				<div class="icon_1 text-center">
					<div class="text">
						Cuidadores<br />
						certificados
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_2_icon_1.png">
					</div>
				</div>
				<div class="icon_2 text-center">
					<div class="text">
						Máx. 6 perros<br />
						por paseo
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_2_icon_2.png">
					</div>
				</div>
				<div class="icon_3 text-center">
					<div class="text">
						Geolocalización
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_2_icon_3.png">
					</div>
				</div>
				<div class="icon_4 text-center">
					<div class="text">
						Seguro en caso<br />
						de accidente
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_2_icon_4.png">
					</div>
				</div>
				<div class="icon_5 text-center">
					<div class="text">
						Acumula<br />
						wakpuntos
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_2_icon_5.png">
					</div>
				</div>
				<div class="main_landing text-center">
					<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_2.png">
				</div>
				<div class="title_1 text-center">
					<div class="text_1">¡OBTÉN BENEFICIOS!</div>
					<div class="text_2">CON ESTOS TRES PASOS</div>
				</div>
			</div>
		</div>
	</section>
<!-- End P Part 2 -->
<!-- Begin P Part 3 -->
	<section class="p_part_3">
		<div class="row">
			<div class="small-12 medium-4 columns">
				<div class="icon_1 text-center">
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_3_icon_1.png">
					</div>
					<div class="text">
						<div class="text_1">DÉJANOS</div>
						<div class="text_2">tus datos</div>
					</div>
				</div>
			</div>
			<div class="small-12 medium-4 columns">
				<div class="icon_2 text-center">
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_3_icon_2.png">
					</div>
					<div class="text">
						<div class="text_1">DESCARGA</div>
						<div class="text_2">la App.</div>
					</div>
				</div>
			</div>
			<div class="small-12 medium-4 columns">
				<div class="icon_3 text-center">
					<div class="image">
						<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_3_icon_3.png">
					</div>
					<div class="text">
						<div class="text_1">ENVÍANOS UNA FOTO DE</div>
						<div class="text_2">tu perrito con un producto pedigree</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<div class="box text-center">
					<div class="text_1">Envía la foto al siguiente correo electrónico:</div>
					<div class="text_2">pedigree@mywak.com.co</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-6 columns text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_3.png">
			</div>
			<div class="small-12 medium-5 columns">
				<div class="form_landing">
					<?php echo do_shortcode( '[contact-form-7 id="1046" title="Paseos"]' ); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End P Part 3 -->
<!-- Begin P Part 4 -->
	<section class="p_part_4">
		<div class="row">
			<div class="small-12 medium-5 columns"></div>
			<div class="small-12 medium-7 columns">
				<div class="image text-center">
					<img src="<?php echo get_template_directory_uri(); ?>/build/p_part_4.png">
				</div>
			</div>
		</div>
	</section>
<!-- End P Part 4 -->